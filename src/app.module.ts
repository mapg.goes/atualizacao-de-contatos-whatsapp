import { Logger, Module } from '@nestjs/common';
import { mkdir, rm } from 'fs/promises';
import { cwd } from 'process';
const StarterConnection = require( './domain/starter-connection/starter-connection')

@Module({
})
export class AppModule {
  constructor() {
        StarterConnection.generateConnction().then(() => {
          console.log('Iniciado o Sistema na porta padrão: 3000')
        }).catch( err => console.log(err) )
    
  }
}
