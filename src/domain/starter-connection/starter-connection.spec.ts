import { Test, TestingModule } from '@nestjs/testing';
import { StarterConnection } from './starter-connection';

describe('StarterConnection', () => {
  let provider: StarterConnection;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StarterConnection],
    }).compile();

    provider = module.get<StarterConnection>(StarterConnection);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
