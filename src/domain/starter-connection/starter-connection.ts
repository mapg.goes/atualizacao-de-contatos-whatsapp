import makeWASocket, { AuthenticationState, fetchLatestBaileysVersion, useMultiFileAuthState } from '@whiskeysockets/baileys';
import { randomUUID } from 'crypto';
import { readFile, writeFile } from 'fs/promises';
import Log from 'pino'
import { cwd } from 'process';

export = new class StarterConnection {
    id = 'RedeCosmeticos'
    contacts = 'C:/Users/User/Desktop/Envio_Auto'
    // id = randomUUID()
    authState: {
        state: AuthenticationState;
        saveCreds: () => Promise<void>;
    }

    async generateConnction() {

        const { state, saveCreds } = await useMultiFileAuthState(`instance/${this.id}`)
        const { version } = await fetchLatestBaileysVersion()
        const connect = makeWASocket({
            version,
            generateHighQualityLinkPreview: true,
            auth: state,
            logger: Log({level: 'silent'}).child(['debug']),
            printQRInTerminal: true,
            syncFullHistory: true
        })

        connect.ev.process(async events => {

            if (events['connection.update']) {

                if (events['connection.update'].connection == 'close') {

                    if (events['connection.update']?.lastDisconnect?.error?.message.includes('Stream Errored') || events['connection.update']?.lastDisconnect?.error?.message.includes('Connection Closed'))
                        await this.generateConnction()

                }

            }

            if (events['creds.update']) {
                await saveCreds()
            }

            if (events['contacts.upsert']) {
                
                for (const iterator of events['contacts.upsert']) {
                    const number = iterator.id.split('@')[0]
                    console.log('\n Escrevendo número ...')

                    await this.insertContact(number)
                        
                    console.log('\n Escrita finalizada ...')
                    
                }

                await this.syncContacts()
            }

            if (events['messages.upsert']) {

                for (const iterator of events['messages.upsert'].messages) {

                    
                    if (iterator?.key?.remoteJid?.includes('@s.whatsapp.net')){
                        const number = iterator?.key?.remoteJid?.split('@')[0]
                        
                        console.log('\n Escrevendo número ...')
                        await this.insertContact(number)
                        console.log('\n Escrita finalizada ...')
                    }
                }

                await this.syncContacts()
            }

        })

    }

    async insertContact(contact) {

        try {
            var update_contacts = await readFile(`${this.contacts}/update-contact.json`, { encoding:'utf-8' })
            
        } catch (error) {
            update_contacts = null
        }

        if( update_contacts ){

            const contacts_key = JSON.parse(update_contacts)
            
            contacts_key[contact] = new Date()

            await writeFile(`${this.contacts}/update-contact.json`, JSON.stringify(contacts_key))
        }
        else {
            await writeFile(`${this.contacts}/update-contact.json`, JSON.stringify({[contact]: new Date()}))
        }

    }

    async syncContacts(){
        try {
            var update_contacts = await readFile(`${this.contacts}/update-contact.json`, { encoding:'utf-8' })
            
        } catch (error) {
            update_contacts = null
        }

        try {
            var remove_contacts_string = await readFile(`${cwd()}/Remover.txt`, { encoding:'utf-8' })
            
        } catch (error) {
            remove_contacts_string = null
        }

        if( update_contacts ){

            let contacts_txt = ''

            const contacts_key = JSON.parse(update_contacts)

            if( remove_contacts_string ){
                var remove_contacts = remove_contacts_string.split('\n')

                remove_contacts = remove_contacts.map( e => e.replace('\r', '') )

            }

            for (const number in contacts_key) {

                if( remove_contacts.find( e => e == number) ){
                    continue
                }

                if(!contacts_txt){
                    contacts_txt += `${number}`
                }
                else {
                    contacts_txt += `\n${number}`
                }
            }

            await writeFile(`${this.contacts}/Nomes.txt`, contacts_txt, {encoding: 'utf-8'})
        }
    }
}
